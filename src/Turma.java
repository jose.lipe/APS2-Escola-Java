import java.util.Random;

public class Turma {
	private String disciplina;
	private Aluno[] alunos;

	public Turma(String disciplina, int quantidadeDeAlunos) {
		this.disciplina = disciplina;
		this.alunos = new Aluno[quantidadeDeAlunos];
	}
	
	public void addAluno(Aluno[] alunos) {
		for (int i = 0; i < alunos.length; i++) {
			this.alunos[i] = alunos[i];
		}
	}
	
	public int numeroDaTurma() {
		Random random = new Random();
		return random.nextInt(100);
	}
	
	public int quantidadeAlunos() {
		return this.alunos.length;
	}

	@Override
	public String toString() {
		String str = "Turma: " + this.numeroDaTurma() + " Disciplina: " + this.disciplina + "\n\n***Alunos\n";
		for (Aluno aluno : alunos) {
			str += aluno + "\n";
		}
		return str;
	}
}
