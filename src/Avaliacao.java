
public class Avaliacao {
	private double nota;
	private String data;
	
	public Avaliacao(double nota, String data) {
		this.nota = nota;
		this.data = data;
	}
	
	@Override
	public String toString() {
		return "Nota: " + nota + " Data: " + data;
	}
}
