
public class Aluno {
	private String nome;
	private String matricula;
	private Avaliacao[] avaliacao;
	private double valorTotalNotas;
	
	public Aluno(String nome, String matricula) {
		this.nome = nome;
		this.matricula = matricula;
		this.avaliacao = new Avaliacao[3];
		this.valorTotalNotas = 0;
	}
	
	public void addAvaliacao(double nota, String data) {
		for (int i = 0; i < this.avaliacao.length; i++) {
			if(this.avaliacao[i] == null) {
				this.avaliacao[i] = new Avaliacao(nota, data);
				break;
			}
		}
		
		this.valorTotalNotas += nota;
	}
	
	public int quantidadeAvaliacoes() {
		int qntd = 0;
		for (Avaliacao avaliacao : this.avaliacao) {
			if(avaliacao == null) {
				continue;
			}
			qntd++;
		}
		
		return qntd;
	}
	
	public double calcularMedia() {
		return this.valorTotalNotas / this.quantidadeAvaliacoes();
	}
	
	public boolean excedeNumeroDeAvaliacao()
	{
		if(this.avaliacao[2] != null) {
			return true;
		}
		return false;
	}
	
	public String mostraAvaliacoes() {
		String str = "";
		for (Avaliacao avaliacao : this.avaliacao) {
			if(avaliacao == null) {
				continue;
			}
			
			str += avaliacao + "\n";
		}
		str += "Média: " + calcularMedia() + "\n";
		
		return str;
	}

	@Override
	public String toString() {
		return " Nome: " + this.nome + " Matrícula: " + this.matricula;
	}
}
