
public class Chamada {
	private Aluno aluno;
	private boolean status;
	
	public Chamada(Aluno aluno, boolean status) {
		this.aluno = aluno;
		this.status = status;
	}

	public String mostrarStatus() {
		if(this.status) {
			return "Presente";
		}
		return "Ausente";
	}
	
	@Override
	public String toString() {
		return "Aluno: " + this.aluno + " - " + mostrarStatus();
	}
	
}
