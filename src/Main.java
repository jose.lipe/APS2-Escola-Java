import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Aluno[] alunos = { new Aluno("João", "100"),
						   new Aluno("Maria", "101"),
						   new Aluno("Jorge", "102"),
						   new Aluno("Amanda", "103"),
						   new Aluno("Paulo", "104")
						 };
		Turma turma = null;
		Aula aula = null;
		
		int op = 0;
		do {
			System.out.println("Bem vindo professor\n\n" +
							   "Escolha uma opção:\n" +
							   "1 - Gerar turma\n" +
							   "2 - Cadastrar Aula\n" +
							   "3 - Fazer chamada\n" +
							   "4 - Fazer Avaliacao\n" +
							   "5 - Encerrar Aula\n" +
							   "0 - Sair");
			op = getScanner().nextInt();
			
			switch (op) {
			case 1:
				turma = gerarTurma(alunos);
				System.out.println("Turma gerada com sucesso!!\n\n" + turma);
				break;
			case 2:
				if(turma == null) {
					System.out.println("Gere uma turma primeiro");
					continue;
				}
				
				if(aula != null) {
					System.out.println("Encerre a aula anterior, antes de cadastrar uma nova");
					continue;
				}
				
				aula = cadastrarAula(turma);
				System.out.println("Aula cadastrada com sucesso!!\n\n" + aula);
				break;
			case 3:
				if(turma == null) {
					System.out.println("Gere uma turma primeiro");
					continue;
				}
				
				if(aula == null) {
					System.out.println("Gere uma aula primeiro");
					continue;
				}
				
				if(aula.chamadaRealizada()) {
					System.out.println("Chamada já realizada!");
					continue;
				}
				
				for (Aluno aluno : alunos) {
					System.out.println("Aluno " + aluno + 
							           "\n\nEscolha:\n1 - Presente\n2 - Ausente\n");
					int presenca = getScanner().nextInt();
					
					boolean status = true;
					
					if(presenca == 2) {
						status = false;
					}
					
					aula.realizarChamada(aluno, status);
				}
				
				System.out.println("Chamada: \n\n" + aula.mostraChamada());
				break;
			case 4:
				if(turma == null) {
					System.out.println("Gere uma turma primeiro");
					continue;
				}
				
				System.out.println("Data: ");
				String data = getScanner().next();
				
				for (Aluno aluno : alunos) {
					if(aluno.excedeNumeroDeAvaliacao()) {
						System.out.println("Aluno já tem 3 avaliações!");
						continue;
					}
					
					System.out.println("Aluno " + aluno);
					System.out.println("Nota: ");
					double nota = getScanner().nextDouble();
					
					aluno.addAvaliacao(nota, data);
					System.out.println(aluno.mostraAvaliacoes());
				}
				break;
			case 5:
				aula = null;
				System.out.println("Aula encerrada, com sucesso!!");
				break;
			case 0:
				System.out.println("Encerrando...");
				break;
			default:
				System.out.println("Opção Inválida!!");
				break;
			}
		} while (op != 0);
		
	}
	
	private static Aula cadastrarAula(Turma turma) {
		// TODO Auto-generated method stub
		System.out.println("Qual a data da Aula?");
		String data = getScanner().next();
		System.out.println("Quais as atividades da aula?");
		String atividades = getScanner().nextLine();
		return new Aula(data, atividades, turma);
	}

	private static Turma gerarTurma(Aluno[] alunos) {
		// TODO Auto-generated method stub
		Turma turma = new Turma("Banco de Dados I", alunos.length);
		
		turma.addAluno(alunos);
		
		return turma;
	}

	private static Scanner getScanner() {
		return new Scanner(System.in);
	}
}
