
public class Aula {
	private String data;
	private String atividades;
	private Turma turma;
	private Chamada[] chamada;
	
	public Aula(String data, String atividades, Turma turma) {
		this.data = data;
		this.atividades = atividades;
		this.turma = turma;
		this.chamada = new Chamada[this.turma.quantidadeAlunos()];
	}
	
	public void realizarChamada(Aluno aluno, boolean status) {
		this.addChamada(new Chamada(aluno, status));
	}
	
	private void addChamada(Chamada chamada) {
		// TODO Auto-generated method stub
		for (int i = 0; i < this.chamada.length; i++) {
			if(this.chamada[i] == null) {
				this.chamada[i] = chamada;
				break;
			}
		}
	}
	
	public boolean chamadaRealizada() {
		if(this.chamada[0] == null) {
			return false;
		}
		return true;
	}
	
	public String mostraChamada() {
		String str = "";
		for (Chamada chamada : this.chamada) {
			str += chamada + "\n";
		}
		return str;
	}

	@Override
	public String toString() {
		return "Com a data: " + this.data + "\nAtividades: " + this.atividades + "\nTurma: " + this.turma.numeroDaTurma();
	}
}
